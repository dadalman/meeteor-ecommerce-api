// Dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// Routes file path
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');

// Database connection
mongoose.connect('mongodb+srv://admin:admin@b106.5dwcu.mongodb.net/meeteor-ecommerce?retryWrites=true&w=majority', {
    useNewUrlParser: true, // To use the new parser
    useUnifiedTopology: true, // To use the new server discover and monitoring engine
    useFindAndModify: false
});

mongoose.connection.once('open', () => console.log('Now Connected to the Database'));

// server setup
const app = express();

// allows resource sharing from all origins
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);


app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${ process.env.PORT || 4000 }`);
});