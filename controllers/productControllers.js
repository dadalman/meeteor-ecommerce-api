const { info } = require('console');
const Product = require('../models/product');


// Create product
module.exports.addProduct = (params) => {
    let product = new Product({
        name: params.name,
        description: params.description,
        price: params.price
    });

    return product.save().then((product, err) => {
        return err ? false : true;
    })
}

// Update product info
module.exports.updateInfo = (productId, data) => {
    let newInfo = {
        name: data.name,
        description: data.description,
        price: data.price,
    };

    return Product.findByIdAndUpdate(productId, newInfo).then((data, err) => {
        return (err) ? false : true;
    })
}

// Change product featured info
module.exports.featured = (productId, data) => {
    let newFeatured = {
        isFeatured: data.isFeatured
    }

    return Product.findByIdAndUpdate(productId, newFeatured).then((data, err) => {
        return (err) ? false : true;
    })
}

// Retrieve all featured products
module.exports.getFeatured = () => {
    return Product.find({ isFeatured: true }).then((result, err) => {
        if(result) {
            return result;
        }else {
            return console.log(err);
        }
        
    })
}

// Retrieve all active products
module.exports.getAllActive = () => {
    return Product.find({ isActive: true }).then((result, err) => {
        if(result) {
            return result;
        }else {
            return console.log(err);
        }
        
    })
}

// Retrieve all products
module.exports.getAll = () => {
    return Product.find({}).then((result, err) => {
        if(result) {
            return result;
        }else {
            return console.log(err);
        }
    })
}

// Archive product
module.exports.archiveProduct = (productId) => {
    let deactivate = {
        isActive: false
    }

    return Product.findByIdAndUpdate(productId, deactivate).then((data, err) => {
        return (err) ? false : true;
    })
}

// Enable product
module.exports.activateProduct = (productId) => {
    let activate = {
        isActive: true
    }

    return Product.findByIdAndUpdate(productId, activate).then((data, err) => {
        return (err) ? false : true;
    })
}

// Retrieve single product
module.exports.singleProduct = (productId) => {
    return Product.findById(productId).then((data, err) => {
        return (err) ? false: data;
    })
}

// Retrieve all product orders
module.exports.allProdOrders = () => {
    return Product.find({}).then((data, err) => {
        let allOrders = [];

        // Pushes orders object to allOrders
        for(let i = 0; i < data.length; i++) {
            for(let j = 0; j < data[i].orders.length; j++){
                allOrders.push(data[i].orders[j]);
            }
        }
        
        if(err) {
            return false;
        }else {
            return allOrders;
        }
    })
}
