const User = require('../models/user');
const Product = require('../models/product');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const user = require('../models/user');
const { get } = require('../routes/productRoutes');
const product = require('../models/product');


// Register new user
module.exports.register = async(params) => {
    let checkUsername = await User.findOne({ username: params.username }).then((data, err) => {
        if(data === null && !err) {
            return true;
        }else {
            return false;
        }
    });

    let checkEmail = await User.findOne({ email: params.email }).then((data, err) => {
        if(data === null && !err) {
            return true;
        }else {
            return false;
        }
    })

    if(checkUsername && checkEmail) {
        let user = new User ({
            fullName: params.fullName,
            username: params.username,
            email: params.email,
            password: bcrypt.hashSync(params.password, 10)
        });

        return user.save().then((user,err) => {
            return (err) ? false: true;
        })
    }else {
        return false;
    }
}

// Login user
module.exports.login = (params) => {
    return User.findOne({ email: params.email }).then(user => {
        if(user === null) {
            return false;
        }

        // Compare password
        const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

        if(isPasswordMatched) {
            return { accessToken: auth.createAccessToken(user.toObject())}
        }else {
            return false;
        }
    })
}

// Get user details
module.exports.get = (params) => {
    return User.findById(params.userId).then(user => {
        user.password = undefined;
        return user;
    })
}

// Set to admin
module.exports.toAdmin = (userId) => {
    let newAdmin = {
        isAdmin: true
    }

    return User.findByIdAndUpdate(userId, newAdmin).then((result, err) => {
        return (err) ? false : true;
    })
}

// Add to cart (non-admin)
module.exports.addToCart = (data) => {
    return User.findById(data.userId).then(user => { 

        newCart = {
            cart: user.cart
        }

        for(let i=0; i < user.cart.length; i++) {
            if(newCart.cart[i].productId === data.productId) {
                newCart.cart[i].totalAmount += data.totalAmount;

                return User.findByIdAndUpdate(data.userId, newCart).then(
                    (result, err) => {
                    return (err) ? false : true;
                })
            }
        }   

        user.cart.push({
            productId: data.productId,
            name: data.name,
            price: data.price,
            totalAmount: data.totalAmount
        });
        return user.save().then((user, err) => {
            if(err) {
                return false;
            }else {
                return true;
            }
        })
    });
}

// Create order (non-admin)
module.exports.createOrder = async(data) => {
    var newOrderId;

    // Save product ID and total orders to user orders
    let userSaveOrder = await User.findById(data.userId).then(user => { 


        user.orders.push({
            userId: data.userId,
            fullName: data.fullName,
            username: data.username,
            email: data.email,
            products: data.products,
            totalPrice: data.totalPrice,
            status: data.status
        });

        return user.save().then((user, err) => {
            if(err) {
                return false;
            }else {
                newOrderId = user.orders[user.orders.length - 1].id;
                
                return true;
            }
        })
    })

    // Save user ID and total amount to product orders
    /* let productSaveOrder = await Product.findById(data.productId).then(product => {
        product.orders.push({
            orderId: newOrderId,
            productId: data.productId,
            name: data.name,
            price: data.price,
            userId: data.userId,
            totalAmount: data.totalAmount
        });
        return product.save().then((product, err) => {
            if(err) {
                return false;
            }else {
                return true;
            }
        })
    }) */

    let productSaveOrder = await {};

    if(userSaveOrder && productSaveOrder) {
        return true;
    }else {
        return false;
    }

}

// Retrieve user's cart
module.exports.getMyCart = (userId) => {
    return User.findById(userId).then((data, err) => {
        if(err) {
            false;
        }else {
            return data.cart;
        }
    })
}

// Change product quantity
/* module.exports.removeProduct = (userId, productId, amount) => {
    return User.findById(userId).then((data, err) => {

        let newCart
        
        newCart = {
            cart: data.cart.filter((product) => {
            return product.productId === productId;
            })
        };
        }

        console.log(newCart);
        
        return User.findByIdAndUpdate(userId, newCart).then((result, err) => {
            return (err) ? false : true;
        })
    })
} */


// Remove from cart
module.exports.removeProduct = (userId, productId) => {
    return User.findById(userId).then((data, err) => {

        let newCart
        
        if(productId === "all") {
            newCart = { cart: [] }
        }else {
            newCart = {
                cart: data.cart.filter((product) => {
                return product.productId !== productId;
                })
            };
        }

        console.log(newCart);
        
        return User.findByIdAndUpdate(userId, newCart).then((result, err) => {
            return (err) ? false : true;
        })
    })
}

// Retrieve user orders
module.exports.getMyOrders = (userId) => {
    return User.findById(userId).then((data, err) => {
        if(err) {
            false;
        }else {
            return data.orders;
        }
    })
}

// Retrieve all orders from all users
module.exports.getAllOrders = () => {
    return User.find({orders:{$ne:[]}}).then((data, err) => 
        {
            console.log(data);

            let allOrders = [];

            for(let i = 0; i < data.length; i++) {
                for(let j = 0; j < data[i].orders.length ; j++) {
                    allOrders.push(data[i].orders[j]);
                }
                
            }

            allOrders = allOrders.sort(function(a, b) {
                var c = new Date(a.purchasedOn);
                var d = new Date(b.purchasedOn);
                return c-d;
            });

            console.log(allOrders);

            if(err) {
                false;
            }else {
                return allOrders;
            }
        }
    )
}

// Retrieve specific user
module.exports.userData = (userData) => {
    
    if(userData.email !== ''){
        return User.find({ email: userData.email }).then((user, err) => {

            return (err) ? false : user;
        });
    }else if(userData.username !== '') {
        return User.find({ username: userData.username }).then((user, err) => {

            return (err) ? false : user;
        });
    }else {
        return User.find({ _id: userData.userId }).then((user, err) => {

            return (err) ? false : user;
        });
    }
};

// Change status
module.exports.changeStatus = (userId, status, orderId) => {

    return User.findById(userId).then(data => {

        let newOrders = {
            orders: [...data.orders]
        }
        
        for(let i = 0; i< newOrders.orders.length; i++) {
            if(newOrders.orders[i].id === orderId) {
                newOrders.orders[i].status = status;
            }
        }

        console.log(newOrders)

        return User.findByIdAndUpdate(userId, newOrders).then((data, err) => {
            return (err) ? false : true;
        })
    })
}

// Remove orders
module.exports.removeOrder = (userId, orderId) => {
    return User.findById(userId).then((data, err)  => {
        let newOrders = {
            orders: data.orders.filter((order) => {
                return order.id !== orderId;
            })
        }

        return User.findByIdAndUpdate(userId, newOrders).then((data, err) => {
            return (err) ? false : true;
        })
    })
}