const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    fullName: {
        type: String,
        required: [true, "Full name is required"]
    },

    username: {
        type: String,
        required: [true, "Username is required"]
    },

    email: {
        type: String,
        required: [true, "Email is required"]
    },

    password: {
        type: String,
        required: [true, "Password is required"]
    },

    isAdmin: {
        type: Boolean,
        default: false
    },

    createdOn: {
        type: Date,
        default: new Date()
    },

    cart: [
        {
            productId: {
                type: String,
                required: [true, "Product ID is required"]
            },
            
            name: {
                type: String,
                required: [true, "Product name is required"]
            },

            price: {
                type: String,
                required: [true, "Product price is required"]
            },

            totalAmount: {
                type: Number,
                required: [true, "Total amount is required"]
            }
        }
    ],

    orders: [
        {
            userId: {
                type: String,
                required: [true, "Product status is required"]
            },

            fullName: {
                type: String,
                required: [true, "Full name is required"]
            },
        
            username: {
                type: String,
                required: [true, "Username is required"]
            },
        
            email: {
                type: String,
                required: [true, "Email is required"]
            },

            products: [{
                productId: {
                    type: String,
                    required: [true, "Product ID is required"]
                },
                
                name: {
                    type: String,
                    required: [true, "Product name is required"]
                },
    
                price: {
                    type: String,
                    required: [true, "Product price is required"]
                },
    
                totalAmount: {
                    type: Number,
                    required: [true, "Total amount is required"]
                }
            }],

            status: {
                type: String,
                required: [true, "Product status is required"]
            },

            totalPrice: {
                type: Number,
                required: [true, "Total price is required"]
            },

            purchasedOn: {
                type: String,
                default: new Date()
            }
        }
    ]
});


module.exports = mongoose.model('User', userSchema);