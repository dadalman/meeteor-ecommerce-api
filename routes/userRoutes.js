const express = require('express');
const router = express.Router();
const UserControllers = require('../controllers/userControllers');
const auth = require('../auth')


// register
router.post('/register', (req, res) => {
    UserControllers.register(req.body).then(result => res.send(result));
});

// login
router.post('/login', (req, res) => {
    UserControllers.login(req.body).then(result => res.send(result));
})

// get details of a user
router.get('/details', auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization);
    UserControllers.get({ userId: user.id }).then(user => res.send(user));
})

// set user as admin
router.put('/toAdmin/:id', auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;
    
    if(isAdmin) {
        UserControllers.toAdmin(req.params.id).then(result => res.send(result));
    }else {
        return res.send(false);
    }
});

// Add to cart (non-admin)
router.post('/cart', auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;

    if(isAdmin) {
        res.send(false)
    }else {
        let data =  {
            userId: auth.decode(req.headers.authorization).id,
            productId: req.body.productId,
            name: req.body.name,
            price: req.body.price,
            totalAmount: req.body.totalAmount
        }

        UserControllers.addToCart(data).then(result => res.send(result));
    }   
})

// Create order (non-admin)
router.post('/order', auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;

    if(isAdmin) {
        res.send(false)
    }else {
        let data =  {
            userId: auth.decode(req.headers.authorization).id,
            fullName: req.body.fullName,
            username: req.body.username,
            email: req.body.email,
            products: req.body.products,
            totalPrice: req.body.totalPrice,
            status: req.body.status
        }

        UserControllers.createOrder(data).then(result => res.send(result));
    }   
})

// Retrieve user's cart
router.get('/myCart', auth.verify, (req, res) => {
    userInfo = auth.decode(req.headers.authorization);

    userId = userInfo.id;
    isAdmin = userInfo.isAdmin;

    if(isAdmin) {
        res.send(false);
    }else {
        UserControllers.getMyCart(userId).then(result => res.send(result));
    }
})



// Change product quantity
router.put('/quantity', auth.verify, (req, res) => {
    userInfo = auth.decode(req.headers.authorization);

    userId = userInfo.id;

    UserControllers.changeQuantity(userId, req.body.productId, req.body.amount).then(result => res.send(result));
})


// Remove from cart
router.delete('/remove', auth.verify, (req, res) => {
    userInfo = auth.decode(req.headers.authorization);

    userId = userInfo.id;
    isAdmin = userInfo.isAdmin;

    if(isAdmin) {
        res.send(false);
    }else {
        UserControllers.removeProduct(userId, req.body.productId).then(result => res.send(result));
    }
})

// Retrieve user orders
router.get('/myOrders', auth.verify, (req, res) => {
    userInfo = auth.decode(req.headers.authorization);
    
    userId = userInfo.id;
    isAdmin = userInfo.isAdmin;

    if(isAdmin) {
        res.send(false);
    }else {
        UserControllers.getMyOrders(userId).then(result => res.send(result));
    }
})

// Retrieve all orders from all user
router.get('/allOrders', auth.verify, (req, res) => {

    UserControllers.getAllOrders().then(result => res.send(result));

    /* if(isAdmin) {
        UserControllers.getAllOrders().then(result => res.send(result));
    }else {
        res.send(false);
    } */
})

// Retrieve specific user
router.post('/user', auth.verify, (req, res) => {
    userInfo = auth.decode(req.headers.authorization);

    isAdmin = userInfo.isAdmin;

    if(isAdmin) {
        UserControllers.userData(req.body).then(data => res.send(data));
    }else {
        res.send(false);
    }
})

//Change status
router.put('/status', auth.verify, (req, res) => {
    userInfo = auth.decode(req.headers.authorization);

    userId = userInfo.id;
    isAdmin = userInfo.isAdmin;

    if(isAdmin){
        UserControllers.changeStatus(req.body.userId, req.body.status, req.body.orderId).then(data => res.send(data));
    }else {
        UserControllers.changeStatus(userId, req.body.status, req.body.orderId).then(data => res.send(data));
    }
    
})

// Remove order
router.delete('/removeOrder', auth.verify, (req, res) => {
    userInfo = auth.decode(req.headers.authorization);
    
    if(userInfo.isAdmin) {
        userId = req.body.userId
    }else {
        userId = userInfo.id
    }
    
    orderId = req.body.orderId;

    UserControllers.removeOrder(userId, orderId).then(data => res.send(data));
})


module.exports = router;