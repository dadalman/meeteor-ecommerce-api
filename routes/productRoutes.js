const express = require('express');
const router = express.Router();
const ProductControllers = require('../controllers/productControllers');
const auth = require('../auth')


// Create product
router.post('/add', auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;

    if(isAdmin) {
        ProductControllers.addProduct(req.body).then(result => res.send(result));
    }else {
        res.send(false);
    }
})

// Update product info
router.put('/update/:id', auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;

    if(isAdmin) {
        ProductControllers.updateInfo(req.params.id, req.body).then(result => res.send(result));
    }else {
        res.send(false);
    }
})

// Change product featured info
router.put('/featured/:id', auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;

    if(isAdmin) {
        ProductControllers.featured(req.params.id, req.body).then(result => res.send(result));
    }else {
        res.send(false);
    }
})

// Retrieve all featured products
router.get('/featured', (req, res) => {
    ProductControllers.getFeatured().then(result => res.send(result));
})

// Retrieve all active products
router.get('/active', (req, res) => {
    ProductControllers.getAllActive().then(result => res.send(result));
})

// Get all products
router.get('/all', (req, res) => {
    ProductControllers.getAll().then(result => res.send(result));
})


// Create order (non-admin)
router.post('/order/:id', auth.verify, (req, res) => {
    let user = auth.decode(req.headers.authorization)
    
    let isAdmin = user.isAdmin;
    let userId = user.id;
    let productId = req.params.id;

    if(isAdmin) {
        res.send(false);
    }else {
        ProductControllers.createOrder(userId, productId,  req.body).then(result => res.send(result));
    }
   
})

// Archive products
router.delete('/archive/:id', auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;

    if(isAdmin) {
        ProductControllers.archiveProduct(req.params.id).then(result => res.send(result));
    }else {
        res.send(false);
    }
})

// Enable products
router.get('/enable/:id', auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;

    if(isAdmin) {
        ProductControllers.activateProduct(req.params.id).then(result => res.send(result));
    }else {
        res.send(false);
    }
})

// Retrieve single product
router.get("/product/:id", (req, res) => {
    ProductControllers.singleProduct(req.params.id).then(result => res.send(result));
})

// Retrieve all product orders
router.get('/orders', auth.verify, (req, res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;
    
    if(isAdmin){
        ProductControllers.allProdOrders().then(result => res.send(result));
    }else {
        res.send(false);
    }
})


module.exports = router;